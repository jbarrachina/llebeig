<?php defined('BASEPATH') OR exit('No direct script access allowed');
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Tienda
 *
 * @author jose
 */
class Tienda extends CI_Model{
    
    public function get_articulos() {
//El primer paso es escribir la consulta y guardarla en la variable $sql
        $sql = <<< SQL
            SELECT * 
              FROM articulos
SQL;
//Ejecutar la consulta
        $consulta = $this->db->query($sql);
//Pasar los resultados al controlador
        /*echo '<pre>';
        print_r($consulta);
        echo '</pre>';*/
        return $consulta->result();
    }
    
    public function add_articulo($articulo){
        //insertamos en la tabla articulos el array donde las claves son los nombres
        //de los campos
        $this->db->insert('articulos',$articulo);
    }
    
    public function  guardar($articulo){
        $this->db->insert('articulos',$articulo);
    }
}
