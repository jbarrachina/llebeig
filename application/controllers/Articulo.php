<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * Clase controladora
 * Contendrá las funciones que gestionarán los diferentes eventos que se produzcan
 * en la aplicación web
 * Cada función será un controlador.
 *
 * @author jose
 */
class Articulo extends CI_Controller {
    
    public function __construct(){
        parent::__construct();
        $this->load->database();
        $this->load->helper(['url']);
        $this->load->model('tienda');
    }
    
    //creamos el controlador por defecto
    public function index(){       
        $data['resultado'] = $this->tienda->get_articulos();
        $this->load->view('articulos/lista',$data);
        /*echo '<pre>';
        print_r($resultado);
        echo '</pre>';*/
    }
    
    //Este es el nuevo controlador
    public function tabla(){
        $data['resultado'] = $this->tienda->get_articulos();
        $this->load->view('articulos/tabla',$data);
    }  
    
    public function alta(){
        $this->load->helper('form');
        $this->load->library('form_validation');
        $datos['titulo'] = 'Alta de Nuevos Artículos';
        $this->load->view('commons/header',$datos);
        //reglas
        $this->form_validation->set_rules('codigo', 'código del artículo', 'required|is_unique[articulos.codigo]|integer|trim');
        $this->form_validation->set_rules('nombre', 'nombre del artículo', 'required|trim');
        $this->form_validation->set_rules('volumen', 'capacidad del artículo', 'required|integer');
        $this->form_validation->set_rules('grados', 'graduación del artículo', 'required|integer');
        $this->form_validation->set_rules('precio', 'precio del artículo', 'required|decimal|greater_than[2]');
        $this->form_validation->set_rules('categoria', 'categoria del artículo', 'required|is_natural_no_zero|in_list[1,2,3]');
        $this->form_validation->set_rules('descripcion', 'descripción del artículo', 'required');
        
        if ($this->form_validation->run() == FALSE) { //no se cumplen las reglas muestra el formulario
            $this->load->view('articulos/ficha_alta');
        }
        else {
            //tratamiento de datos, datos validados
            $articulo = [
                'codigo' => $this->input->post('codigo'),
                'nombre' => $this->input->post('nombre'),
                'volumen' => $this->input->post('volumen'),
                'grados' => $this->input->post('grados'),
                'precio' => $this->input->post('precio'),
                'categoria' => $this->input->post('categoria'),
                'descripcion' => $this->input->post('descripcion')
            ];
            $this->tienda->add_articulo($articulo);
        }      
        $this->load->view('commons/footer');
    }
    
    
    public function alta_beta(){
        $this->load->helper('form');
        //primer paso
        $this->load->library('form_validation');
        //establecer las reglas
        $this->form_validation->set_rules('codigo','El código del Artículo','required|integer|is_unique[articulos.codigo]');
        $this->form_validation->set_rules('nombre','El nombre del Artículo','required|trim');
//diferencia entre el "bien" y el "mal"        
        if($this->form_validation->run()===FALSE){
            $datos['titulo'] = 'Alta de Nuevos Artículos';
            $this->load->view('commons/header',$datos);
            $this->load->view('articulos/ficha_alta_beta');
            $this->load->view('commons/footer');
        } else {
            $datos['titulo'] = 'Éxito: Artículo  cvc guardado';
            $this->load->view('commons/header',$datos);
            $articulo = [
               'nombre' => $this->input->post('nombre'),
                'precio' => $this->input->post('precio'),
                'codigo' => $this->input->post('codigo'),
                'volumen' => $this->input->post('volumen'),
                'grados' => $this->input->post('grados'),
                'categoria' => $this->input->post('categoria'),
                'descripcion' => $this->input->post('descripcion'),
            ];
            $this->tienda->guardar($articulo);
            $this->load->view('commons/footer');
        }
    }  
    
    public function guardar(){
        
    }

}
