<div class="bg-danger">
   <?php echo validation_errors(); ?> 
</div>

<div>
    <?php echo form_open(site_url('articulo/show_form'),['class' => 'form-horizontal']);?>
    <div class="form-row">
        <div class="col-md-2">
            <?php echo form_label('Código: ', 'codigo', ['class' => 'control-label']); ?>
            <?php $marca = form_error('codigo')!==''?'border-danger bg-warning':'';?>
            <?php echo form_input(['name' => 'codigo', 'id' => 'codigo', 'class' => "form-control $marca", 'placeholder' => 'Código artículo', 'value'=> set_value('codigo')]); ?>
            <?php echo form_error('codigo','<div class="small text-danger">', '</div>');?>
        </div>
        <div class="col-md-6">
            <?php echo form_label('Nombre: ','nombre',['class' => 'control-label']);?>
            <?php echo form_input(['name'=>'nombre', 'id'=>'nombre', 'class' => 'form-control', 'placeholder'=>'Introduce nombre artículo']);?>
        </div>    
    </div>
    <div class="form-row">    
        <div class="col-md-2">
            <?php echo form_label('Capacidad: ', 'volumen', ['class' => 'control-label']); ?>
            <?php echo form_input(['name' => 'volumen', 'id' => 'volumen', 'class' => 'form-control', 'placeholder' => 'volumen artículo']); ?>
        </div>
        <div class="col-md-2">
            <?php echo form_label('Grados: ','grados',['class' => 'control-label']);?>
            <?php echo form_input(['name'=>'grados', 'id'=>'grados', 'class' => 'form-control', 'placeholder'=>'Graduación']);?>
        </div>
        <div class="col-md-2">
            <?php echo form_label('Catergoría: ', 'categoria', ['class' => 'control-label']); ?>
            <?php echo form_input(['name' => 'categoria', 'id' => 'categoria', 'class' => 'form-control', 'placeholder' => 'Categoría']); ?>
        </div>
        <div class="col-md-2">
            <?php echo form_label('Precio: ','precio',['class' => 'control-label']);?>
            <?php echo form_input(['name'=>'precio', 'id'=>'precio', 'class' => 'form-control', 'placeholder'=>'Introduce precio']);?>
        </div>  
    </div> 
    <div class="form-row">    
        <div class="col-md-8">
            <?php echo form_label('Descripción: ','descripcion',['class' => 'control-label']);?>
            <?php echo form_input(['name'=>'descripcion', 'id'=>'descripcion', 'class' => 'form-control', 'placeholder'=>'Introduce la descripción']);?>
        </div>
    </div>
    <div class="form-row">    
        <div class="col-md-8">
        <?php echo form_submit('Nuevo', 'Enviar', ['class'=> 'btn btn-primary']);?>
        </div>
    </div>
    <?php echo form_close();?>
</div>

