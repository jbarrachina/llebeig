
<?php echo form_open(site_url('articulo/alta_beta'), ['class'=>'form-horizontal']);?>
    <div class="form-row">
        <div class="col-2">
            <?php echo form_label('Código:','codigo');?>
            <?php $marca = form_error('codigo')!== '' ? 'border-danger bg-warning':'';?>
            <?php echo form_input(['name'=>'codigo','id'=>'codigo', 'class'=>"form-control $marca", 'value'=>set_value('codigo')]);?>
            <?php echo form_error('codigo','<div class="small text-danger">','</div>');?>
        </div>
        <div class="col-6">
            <?php echo form_label('Nombre:','nombre');?>
            <?php echo form_input(['name'=>'nombre','id'=>'nombre', 'class'=>'form-control','placeholder'=>'Introduce el nombre del artículo', 'value'=>set_value('nombre')]);?>
        </div>    
    </div>  
    <div class="form-row">
        <div class="col-2">
            <?php echo form_label('Volumen:','volumen');?>
            <?php echo form_input(['name'=>'volumen','id'=>'volumen', 'class'=>'form-control', 'value'=>set_value('volumen',70)]);?>
        </div>
        <div class="col-2">
            <?php echo form_label('Precio:','precio');?>
            <?php echo form_input(['name'=>'precio','id'=>'precio', 'class'=>'form-control', 'value'=>set_value('precio')]);?>
        </div>
        <div class="col-2">
            <?php echo form_label('Graduación:','grados');?>
            <?php echo form_input(['name'=>'grados','id'=>'grado', 'class'=>'form-control', 'value'=>set_value('grados')]);?>
        </div>
        <div class="col-2">
            <?php echo form_label('Categoría:','categoria');?>
            <?php echo form_input(['name'=>'categoria','id'=>'categoria', 'class'=>'form-control', 'value' => set_value('categoria')]);?>
        </div>
    </div>
    <div class="form-row">
        <div class="col-8">
            <?php echo form_label('Descripción:','descripcion');?>
            <?php echo form_input(['name'=>'descripcion','id'=>'descripcion', 'class'=>'form-control', 'value'=>set_value('descripcion')]);?>
        </div>   
    </div>
    <div class="form-row">
        <div class="col-8">
            <?php echo form_submit('enviar', 'Guardar');?>
        </div>   
    </div>
<?php echo form_close();?>

