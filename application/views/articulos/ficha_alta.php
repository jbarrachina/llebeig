<?php defined('BASEPATH') OR exit('No direct script access allowed');?> 
<div class="bg-warning">
    <?php echo validation_errors(); ?>
</div>    
<?php echo form_open('articulo/alta', ['class' => 'form-horizontal']); ?>
     <div class="form-row">
        <div class="col-md-2 mb-1">
            <?php echo form_label('Código: ', 'codigo', ['class' => 'control-label']); ?>
            <?php echo form_input(['id'=>'codigo', 'name' => 'codigo', 'class' => 'form-control', 'placeholder' => 'codigo', 'value' => set_value('codigo',''),]); ?>
        </div>
        <div class="col-md-6 mb-3">
            <?php echo form_label('Nombre: ', 'nombre', ['class' => 'control-label']); ?>
            <?php echo form_input(['id'=>'nombre','name' => 'nombre', 'class' => 'form-control', 'placeholder' => 'Nombre del artículo','value' => set_value('nombre',''),]); ?>
        </div>
     </div>
     <div class="form-row">
        <div class="col-md-2 mb-1"> 
            <?php echo form_label('Capacidad: ', 'volumen', ['class' => 'control-label']); ?>       
            <?php echo form_input(['id'=>'volumen','name' => 'volumen', 'class' => 'form-control', 'placeholder' => 'volumen en ml','value' => set_value('volumen','70'),]); ?>
        </div>
        <div class="col-md-2 mb-1"> 
            <?php echo form_label('Graduación: ', 'grados', ['class' => 'control-label']); ?>         
            <?php echo form_input(['id'=>'grados','name' => 'grados', 'class' => 'form-control', 'placeholder' => 'grados','value' => set_value('grados',''),]); ?>
        </div>
        <div class="col-md-2 mb-1"> 
            <?php echo form_label('Categoría: ', 'categoria', ['class' => 'control-label']); ?>
            <?php echo form_input(['id'=>'categoria','name' => 'categoria', 'class' => 'form-control', 'placeholder' => 'tipo de artículo: 1, 2 ó 3','value' => set_value('categoria','1'),]); ?>
        </div>
        <div class="col-md-2 mb-1">
            <?php echo form_label('Precio: ', 'precio', ['class' => 'control-label']); ?>
            <?php echo form_input(['id'=>'precio','name' => 'precio', 'class' => 'form-control', 'placeholder' => 'precio de venta al público','value' => set_value('precio',''),]); ?>
        </div>
    </div>
    <div class="form-row">
        <div class="col-md-8 mb-4">
            <?php echo form_label('Descripción: ', 'descripcion', ['class' => 'control-label']); ?>
            <?php echo form_input(['id'=>'descripcion','name' => 'descripcion', 'class' => 'form-control', 'placeholder' => 'Descripción del artículo','value' => set_value('descripcion',''),]); ?>
        </div>    
    </div>
    <div class="form-row">
        <div class="col-md-2 mb-1">
            <?php echo form_submit('Alta', 'Enviar', ['class'=>'btn btn-primary']); ?>
        </div>
    </div>
<?php echo form_close(); ?>

