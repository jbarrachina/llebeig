<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>

<html>
    <head>
        <meta charset="UTF-8">
        <link rel="stylesheet" href="<?php echo base_url('assets/css/bootstrap.min.css');?>">
        <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/DataTables-1.10.18/css/dataTables.bootstrap4.min.css');?>"/>
 
        <script type="text/javascript" src="<?php echo base_url('assets/jQuery-3.3.1/jquery-3.3.1.min.js');?>"></script>
        <script type="text/javascript" src="<?php echo base_url('assets/DataTables-1.10.18/js/jquery.dataTables.min.js');?>"></script>
        <script type="text/javascript" src="<?php echo base_url('assets/DataTables-1.10.18/js/dataTables.bootstrap4.min.js');?>"></script>
        <script type="text/javascript" src="<?php echo base_url('assets/js/tienda.js');?>"></script>
        <title>Artículos</title>
    </head>
    <body>
        <div class="container">
            <div class="row d-flex justify-content-center">
                <h1 class="text-white bg-danger col-8">
                    Lista de Artículos
                </h1>
            </div>    
            <div class="row d-flex justify-content-center">  
                <table class="table table-striped table-condensed dataTable">
                    <thead>
                        <th class="col-md-1"></th>
                        <th class="col-md-4">Artículo</th>
                        <th class="col-md-1">Precio</th>
                        <th class="col-md-1">Volumne</th>
                        <th class="col-md-1">Categoría</th>
                    </thead>
                    <tbody>
                    <?php foreach ($resultado as $articulo): ?>
                        <tr>
                            <td>
                                <img width="30px" src="<?php echo base_url('assets/images/articles/'.$articulo->codigo.'.jpg');?>" alt="<?php echo $articulo->nombre; ?>"> 
                            </td>
                            <td>
                               <?php echo $articulo->nombre; ?>
                            </td>
                            <td>
                               <?php echo $articulo->precio; ?>
                            </td>
                            <td>
                               <?php echo $articulo->volumen; ?>
                            </td>
                            <td>
                               <?php echo $articulo->categoria; ?>
                            </td>
                        </tr>
                    <?php endforeach;?>
                    </tbody>    
                </table>
            </div>    
        </div>    
    </body>
</html>
